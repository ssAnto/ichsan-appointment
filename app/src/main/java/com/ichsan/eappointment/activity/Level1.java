package com.ichsan.eappointment.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ichsan.eappointment.R;
import com.ichsan.eappointment.constant.Constanta;
import com.ichsan.eappointment.retrofit.APIUtilities;
import com.ichsan.eappointment.retrofit.RequestAPIServices;
import com.ichsan.eappointment.retrofit.model.DatumDosen;
import com.ichsan.eappointment.retrofit.model.GlobalResponse;
import com.ichsan.eappointment.retrofit.model.HistoryAppointment;
import com.ichsan.eappointment.retrofit.model.ListAppointment;
import com.ichsan.eappointment.retrofit.model.ListDosen;
import com.ichsan.eappointment.retrofit.model.ListKetersediaan;
import com.ichsan.eappointment.retrofit.model.ListUser;
import com.ichsan.eappointment.utility.ChangeActivity;
import com.ichsan.eappointment.utility.Loading;
import com.ichsan.eappointment.utility.SessionManager;
import com.ichsan.eappointment.utility.SpinnerSetter;
import com.ichsan.eappointment.utility.adapter.ListAppointmentAdapter;
import com.ichsan.eappointment.utility.adapter.ListHistoryAdapter;
import com.ichsan.eappointment.utility.adapter.ListKetersediaanDosen;
import com.ichsan.eappointment.utility.adapter.ListUserAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Level1 extends AppCompatActivity {

    private Context context = this;
    private FrameLayout fl;
    private  String buttonKe;
    private String position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);

        if(getIntent().getExtras()==null){
            // jika tidak ada nilai, lempar ke login
            ChangeActivity.To(context,MainActivity.class);
        }
        Bundle i = getIntent().getExtras();
        buttonKe = i.getString(Constanta.CODE);
        position = SessionManager.getRole(context);

        if(position.equals("1") && buttonKe.equals("1")){
            fl = findViewById(R.id.adm_button_1);
            fl.setVisibility(View.VISIBLE);
            actionAdmBtn1();
        }else if(position.equals("1") && buttonKe.equals("2")){
            fl = findViewById(R.id.list_adm_dsn_mhs);
            fl.setVisibility(View.VISIBLE);
            actionAdmBtn2();
        }else if(position.equals("2") && buttonKe.equals("2")){
            fl = findViewById(R.id.list_adm_dsn_mhs);
            fl.setVisibility(View.VISIBLE);
            actionDsnBtn2();
        }else if(position.equals("2") && buttonKe.equals("3")){
            fl = findViewById(R.id.list_adm_dsn_mhs);
            fl.setVisibility(View.VISIBLE);
            actionDsnBtn3MhsBtn2(getString(Constanta.TxtBTN3[Integer.parseInt(position)]));
        }else if(position.equals("3") && buttonKe.equals("1")){
            fl = findViewById(R.id.mhs_button_1);
            fl.setVisibility(View.VISIBLE);
            actionMhsBtn1();
        }else if(position.equals("3") && buttonKe.equals("2")){
            fl = findViewById(R.id.list_adm_dsn_mhs);
            fl.setVisibility(View.VISIBLE);
            actionDsnBtn3MhsBtn2(getString(Constanta.TxtBTN2[Integer.parseInt(position)]));
        }else if(position.equals("3") && buttonKe.equals("3")){
            fl = findViewById(R.id.list_adm_dsn_mhs);
            fl.setVisibility(View.VISIBLE);
            actionListKetersediaan(getString(Constanta.TxtBTN3[Integer.parseInt(position)]));
        }else{
            ChangeActivity.To(context,MainActivity.class);
        }

    }

    private void actionListKetersediaan(String s) {
        RecyclerView rv;
        TextView title;
        FrameLayout info;

        title = findViewById(R.id.title_list);
        rv = findViewById(R.id.list);
        info = findViewById(R.id.info);
        title.setText(s);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(layoutManager);
        ////////////////////////////

        ////// LIST KETERSEDIAAN ////////////
        ProgressDialog l = Loading.get(context);
        l.show();
        RequestAPIServices x = APIUtilities.getAPIServices();
        x.list_availability_dosen().enqueue(new Callback<ListKetersediaan>() {
            @Override
            public void onResponse(Call<ListKetersediaan> call, Response<ListKetersediaan> response) {
                l.dismiss();
                if(response.code()==200){
                    if(response.body().getStatus().equals(Constanta.SUCCESS)){
                        if(response.body().getData()!=null){
                            ListKetersediaanDosen a = new ListKetersediaanDosen(response.body().getData(),context);
                            rv.setAdapter(a);
                            rv.setVisibility(View.VISIBLE);
                            a.notifyDataSetChanged();
                            info.setVisibility(View.GONE);
                        }else{
                            rv.setVisibility(View.GONE);
                            info.setVisibility(View.VISIBLE);
                        }
                    }else{
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListKetersediaan> call, Throwable t) {
                l.dismiss();
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        ChangeActivity.To(context,home.class);
    }

    private void actionMhsBtn1() {
        //// INIT /////
        Spinner nama_dsn;
        TextView nip_dsn;
        EditText keterangan;
        Button submit;

        nama_dsn = findViewById(R.id.nama_dsn);
        nip_dsn = findViewById(R.id.text2);
        keterangan = findViewById(R.id.text3);
        submit = findViewById(R.id.button1);
        ////////// GET LIST DOSEN /////////////////

        ProgressDialog l = Loading.get(context);
        l.show();
        RequestAPIServices x = APIUtilities.getAPIServices();
        x.list_dosen().enqueue(new Callback<ListDosen>() {
            @Override
            public void onResponse(Call<ListDosen> call, Response<ListDosen> response) {
                l.dismiss();
                if(response.code()==200){
                    if(response.body().getStatus().equals(Constanta.SUCCESS)){
                        ArrayList<DatumDosen> xl = response.body().getData();
                        Constanta.tmp = xl;
                        String[] x = new String[xl.size()];
                        for (int i = 0; i <response.body().getData().size() ; i++) {
                            x[i] = xl.get(i).getNama_dsn();
                        }
                        SpinnerSetter.Set(context,x,nama_dsn);
                    }else{
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListDosen> call, Throwable t) {
                l.dismiss();
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });

        nama_dsn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nip_dsn.setText(Constanta.tmp.get(position).getNip());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///////// VERIFIKASI INPUT ///////////////
                String error = "";
                if(keterangan.getText().toString().isEmpty()){
                    error+= "- Keterangan Appointment Harus diisi !";
                }
                if(error.equals("")){
                    l.show();
                    x.request_appointment(SessionManager.getUname(context),
                            nip_dsn.getText().toString(),
                            keterangan.getText().toString()).enqueue(new Callback<GlobalResponse>() {
                        @Override
                        public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                            l.dismiss();
                            if(response.code()==200){
                                if(response.body().getStatus().equals(Constanta.SUCCESS)){
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    ChangeActivity.To(context,home.class);
                                }else{
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GlobalResponse> call, Throwable t) {
                            l.dismiss();
                            Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(context,error, Toast.LENGTH_SHORT).show();
                }
            }
        });

        //////////////
    }

    private void actionDsnBtn3MhsBtn2(String s) {
        ///// Init ///////////////
        RecyclerView rv;
        TextView title;
        FrameLayout info;

        title = findViewById(R.id.title_list);
        rv = findViewById(R.id.list);
        info = findViewById(R.id.info);
        title.setText(s);
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(layoutManager);
        ////////////////////////////

        ////// LIST HISTORY////////////
        ProgressDialog l = Loading.get(context);
        l.show();
        RequestAPIServices x = APIUtilities.getAPIServices();
        x.history_appointment(SessionManager.getUname(context),
                SessionManager.getRole(context)).enqueue(new Callback<HistoryAppointment>() {
            @Override
            public void onResponse(Call<HistoryAppointment> call, Response<HistoryAppointment> response) {
                l.dismiss();
                if(response.code()==200){
                    if(response.body().getStatus().equals(Constanta.SUCCESS)){
                        if(response.body().getData()!=null){
                            ListHistoryAdapter a = new ListHistoryAdapter(response.body().getData());
                            rv.setAdapter(a);
                            rv.setVisibility(View.VISIBLE);
                            a.notifyDataSetChanged();
                            info.setVisibility(View.GONE);
                        }else{
                            rv.setVisibility(View.GONE);
                            info.setVisibility(View.VISIBLE);
                        }
                    }else{
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HistoryAppointment> call, Throwable t) {
                l.dismiss();
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void actionDsnBtn2() {
        ///// Init ///////////////
        RecyclerView rv;
        TextView title;
        FrameLayout info;

        title = findViewById(R.id.title_list);
        rv = findViewById(R.id.list);
        info = findViewById(R.id.info);
        title.setText(getString(Constanta.TxtBTN2[Integer.parseInt(position)]));
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(layoutManager);
        ////////////////////////////

        ////// LIST APPOINTMENT////////////
        ProgressDialog l = Loading.get(context);
        l.show();
        RequestAPIServices x  = APIUtilities.getAPIServices();
        x.list_appointment(SessionManager.getUname(context)).enqueue(new Callback<ListAppointment>() {
            @Override
            public void onResponse(Call<ListAppointment> call, Response<ListAppointment> response) {
                l.dismiss();
                if(response.code()==200){
                    if(response.body().getStatus().equals(Constanta.SUCCESS)){
                        if(response.body().getData()!=null){
                            ListAppointmentAdapter a = new ListAppointmentAdapter(response.body().getData());
                            rv.setAdapter(a);
                            a.notifyDataSetChanged();
                            rv.setVisibility(View.VISIBLE);
                            info.setVisibility(View.GONE);
                        }else{
                            rv.setVisibility(View.GONE);
                            info.setVisibility(View.VISIBLE);
                        }
                    }else{
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListAppointment> call, Throwable t) {
                l.dismiss();
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });
        ////////////////////////////////////


    }

    private void actionAdmBtn1() {
        /////////init
        TextView title;
        EditText et1,et2,et3,et4;
        Spinner role;
        Button submit;
        title = findViewById(R.id.title_adm);
        et1 = findViewById(R.id.edit1);
        et2 = findViewById(R.id.edit2);
        et3 = findViewById(R.id.edit3);
        et4 = findViewById(R.id.edit4);
        submit = findViewById(R.id.submit);
        role = findViewById(R.id.role);
        submit.setText("Register");
        title.setText(getString(Constanta.TxtBTN1[Integer.parseInt(position)]));
        SpinnerSetter.Set(context,Constanta.OPSI_REGISTER_USER,role);
        /////////////

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //////// Validasi Input ///////////
                String error = "";
                if(et1.getText().toString().isEmpty()){
                    error = error +" - UserID wajib diisi !\n";
                }
                if(et2.getText().toString().isEmpty()){
                    error = error +" - Nama wajib diisi !\n";
                }
                if(et3.getText().toString().isEmpty()){
                    error = error +" - Password wajib diisi !\n";
                }
                if(!et3.getText().toString().equals(et4.getText().toString())){
                    error = error +" - Password dan Confirm Password tidak sama !\n";
                }
                if(role.getSelectedItemPosition()==0){
                    error = error +" - Role harus dipilih !\n";
                }
                if(error.equals("")){
                    ///////////// Panggil API Register //////////////
                    ProgressDialog l = Loading.get(context);
                    l.show();
                    RequestAPIServices x = APIUtilities.getAPIServices();
                    x.regis_user(et1.getText().toString(),
                            et3.getText().toString(),
                            et2.getText().toString(),
                            String.valueOf(role.getSelectedItemPosition())).enqueue(new Callback<GlobalResponse>() {
                        @Override
                        public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                            l.dismiss();
                            if(response.code()==200){
                                if(response.body().getStatus().equals(Constanta.SUCCESS)){
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    ChangeActivity.To(context,home.class);
                                }else{
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GlobalResponse> call, Throwable t) {
                            l.dismiss();
                            Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                        }
                    });
                    ////////////////////////////////////////////////
                }else {
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }
                //////////////////////////////////
            }
        });
    }

    private void actionAdmBtn2() {
        ///// Init ///////////////
        RecyclerView rv;
        TextView title;
        FrameLayout info;

        title = findViewById(R.id.title_list);
        rv = findViewById(R.id.list);
        info = findViewById(R.id.info);
        title.setText(getString(Constanta.TxtBTN2[Integer.parseInt(position)]));
        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(layoutManager);
        ////////////////////////////

        //// LIST USER /////////////
        ProgressDialog l = Loading.get(context);
        l.show();
        RequestAPIServices x = APIUtilities.getAPIServices();
        x.list_user(SessionManager.getUname(context),
                SessionManager.getRole(context)).enqueue(new Callback<ListUser>() {
            @Override
            public void onResponse(Call<ListUser> call, Response<ListUser> response) {
                l.dismiss();
                if(response.code()==200){
                    if(response.body().getStatus().equals(Constanta.SUCCESS)){

                        if(response.body().getData()!=null) {
                            ListUserAdapter a = new ListUserAdapter(response.body().getData());
                            rv.setAdapter(a);
                            a.notifyDataSetChanged();
                            rv.setVisibility(View.VISIBLE);
                            info.setVisibility(View.GONE);
                        }else{
                            rv.setVisibility(View.GONE);
                            info.setVisibility(View.VISIBLE);
                        }

                    }else{
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ListUser> call, Throwable t) {
                l.dismiss();
                rv.setVisibility(View.GONE);
                info.setVisibility(View.VISIBLE);
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });
        /////////////////////////////////
    }
}
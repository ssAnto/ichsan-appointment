package com.ichsan.eappointment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ichsan.eappointment.R;
import com.ichsan.eappointment.constant.Constanta;
import com.ichsan.eappointment.retrofit.APIUtilities;
import com.ichsan.eappointment.retrofit.RequestAPIServices;
import com.ichsan.eappointment.retrofit.model.DatumDetailAppointment;
import com.ichsan.eappointment.retrofit.model.DetailAppointment;
import com.ichsan.eappointment.retrofit.model.DetailHistory;
import com.ichsan.eappointment.retrofit.model.GlobalResponse;
import com.ichsan.eappointment.utility.ChangeActivity;
import com.ichsan.eappointment.utility.DateTimeConverter;
import com.ichsan.eappointment.utility.Loading;
import com.ichsan.eappointment.utility.SessionManager;
import com.ichsan.eappointment.utility.SpinnerSetter;

import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Level2 extends AppCompatActivity {

    private Context context = this;
    private String position,data;
    private String pointer;
    private FrameLayout fl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level2);

        if(getIntent().getExtras()==null){
            ChangeActivity.To(context,home.class);
        }
        data = getIntent().getExtras().getString(Constanta.CODE);
        position = SessionManager.getRole(context);

        pointer = data.split(" ")[0];

        if(position.equals("1") && pointer.equals("2")){
            // ADMIN Button 2
            fl = findViewById(R.id.adm_button_2);
            fl.setVisibility(View.VISIBLE);
            actionDetailAdmBtn2();
        }else if(position.equals("2") && pointer.equals("2")){
            fl = findViewById(R.id.dsn_button_2);
            fl.setVisibility(View.VISIBLE);
            actionDetailDsnBtn2();
        }else if(position.equals("2") && pointer.equals("3")){
            fl = findViewById(R.id.mhs_dsn_history);
            fl.setVisibility(View.VISIBLE);
            actionDetailHistory();
        }else if(position.equals("3") && pointer.equals("2")){
            fl = findViewById(R.id.mhs_dsn_history);
            fl.setVisibility(View.VISIBLE);
            actionDetailHistory();
        }

    }

    private void actionDetailHistory() {
        TextView nama,nip,keterangan,tgl,shift,status;
        nama = findViewById(R.id.text1h);
        nip = findViewById(R.id.text2h);
        keterangan = findViewById(R.id.text3h);
        tgl = findViewById(R.id.text4h);
        shift = findViewById(R.id.text5h);
        status = findViewById(R.id.text6h);
        ProgressDialog l = Loading.get(context);
        l.show();
        RequestAPIServices x = APIUtilities.getAPIServices();
        x.detail_history_appointment(data.split(" ")[1],position).enqueue(new Callback<DetailHistory>() {
            @Override
            public void onResponse(Call<DetailHistory> call, Response<DetailHistory> response) {
                l.dismiss();
                if (response.code() == 200) {
                    if (response.body().getStatus().equals(Constanta.SUCCESS)) {
                        if (response.body().getData() != null) {
                            nama.setText(response.body().getData().getNama_mhs());
                            nip.setText(response.body().getData().getUser_id_mhs());
                            keterangan.setText(response.body().getData().getKeterangan());
                            tgl.setText(response.body().getData().getTgl_appointment());
                            shift.setText(response.body().getData().getShift());
                            status.setText(Constanta.STATUS_APPROVAL[Integer.parseInt(response.body().getData().getStatus())]);
                            status.setCompoundDrawablesWithIntrinsicBounds(Constanta.GAMBARSTATUS[Integer.parseInt(response.body().getData().getStatus())],0,0,0);
                        } else {
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context,"Error : "+response.code(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<DetailHistory> call, Throwable t) {
                l.dismiss();
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void actionDetailDsnBtn2() {
        String id_appointment = data.split(" ")[1];
        TextView nama,nip,keterangan,tgl_appointment;
        Spinner shift = findViewById(R.id.shift);
        Button approve = findViewById(R.id.button1);
        Button reject = findViewById(R.id.button2);
        nama = findViewById(R.id.text1);
        nip = findViewById(R.id.text2);
        keterangan = findViewById(R.id.text3);
        tgl_appointment = findViewById(R.id.text4);
        SpinnerSetter.Set(context,Constanta.SHIFT,shift);
        ProgressDialog l = Loading.get(context);
        RequestAPIServices y = APIUtilities.getAPIServices();
        y.detail_appointment(id_appointment).enqueue(new Callback<DetailAppointment>() {
            @Override
            public void onResponse(Call<DetailAppointment> call, Response<DetailAppointment> response) {
                l.dismiss();
                if(response.code()==200){
                    if(response.body().getStatus().equals(Constanta.SUCCESS)){
                        if(response.body().getData()!=null) {
                            DatumDetailAppointment tmp = response.body().getData();
                            nama.setText(tmp.getNama_mhs());
                            nip.setText(tmp.getNip());
                            keterangan.setText(tmp.getKeterangan());
                        }else{
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DetailAppointment> call, Throwable t) {
                l.dismiss();
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });

        tgl_appointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                java.util.Calendar today = java.util.Calendar.getInstance();
                final int yearNow = today.get(java.util.Calendar.YEAR);
                final int monthNow = today.get(java.util.Calendar.MONTH);
                final int dayNow = today.get(java.util.Calendar.DATE);
                DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.CustomDatePicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        java.util.Calendar selected = java.util.Calendar.getInstance();
                        selected.set(year,month,dayOfMonth);

                        //konversi ke string
                        SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
                        String tanggal = formatDate.format(selected.getTime());

                        tgl_appointment.setText(tanggal);
                    }
                }, yearNow, monthNow,dayNow );
                datePickerDialog.getDatePicker().setMinDate(new Date().getTime());
                datePickerDialog.getDatePicker().setSpinnersShown(true);
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.show();
            }
        });

        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String error = "";
                if(tgl_appointment.getText().toString().equals("DD-MM-YYYY")){
                    error = error +" - Tgl Appointment wajib diisi !\n";
                }
                if(shift.getSelectedItemPosition()==0){
                    error = error +" - Shift wajib diisi !\n";
                }
                if(error.equals("")){

                    l.show();
                    RequestAPIServices x = APIUtilities.getAPIServices();
                    x.approval_appointment(id_appointment,
                            "1",
                            DateTimeConverter.forSave(tgl_appointment.getText().toString().trim()),
                            String.valueOf(shift.getSelectedItemPosition())).enqueue(new Callback<GlobalResponse>() {
                        @Override
                        public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                            l.dismiss();
                            if(response.code()==200){
                                if(response.body().getStatus().equals(Constanta.SUCCESS)){
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    ChangeActivity.To(context,Level1.class,pointer);
                                }else{
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GlobalResponse> call, Throwable t) {
                            l.dismiss();
                            Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                }
            }
        });

        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String error = "";
                if(error.equals("")){
                    ProgressDialog l = Loading.get(context);
                    l.show();
                    RequestAPIServices x = APIUtilities.getAPIServices();
                    x.approval_appointment(id_appointment,
                            "0",
                            DateTimeConverter.forSave(tgl_appointment.getText().toString().trim()),
                            "0").enqueue(new Callback<GlobalResponse>() {
                        @Override
                        public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                            l.dismiss();
                            if(response.code()==200){
                                if(response.body().getStatus().equals(Constanta.SUCCESS)){
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    ChangeActivity.To(context,Level1.class,pointer);
                                }else{
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GlobalResponse> call, Throwable t) {
                            l.dismiss();
                            Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void actionDetailAdmBtn2() {
        ////// INIT /////////////////////////////////
        String[] tmp = data.split(" ");
        EditText et1,et2,et3,et4;
        Button submit;
        Spinner role;
        et1=findViewById(R.id.edit1);
        et2=findViewById(R.id.edit2);
        et3=findViewById(R.id.edit3);
        et4=findViewById(R.id.edit4);
        role=findViewById(R.id.role);
        submit=findViewById(R.id.submit);
        SpinnerSetter.Set(context,Constanta.OPSI_SETTING_USER,role);
        et1.setText(tmp[1]);
        et2.setText(tmp[2]);
        role.setSelection(Integer.parseInt(tmp[3]));
        ////////////////////////////////////////////

        /////// SETTING USER ////////////////////////////
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //////// Validasi Input ///////////
                String error = "";
                if(et1.getText().toString().isEmpty()){
                    error = error +" - UserID wajib diisi !\n";
                }
                if(et2.getText().toString().isEmpty()){
                    error = error +" - Nama wajib diisi !\n";
                }
                if(et3.getText().toString().isEmpty()){
                    error = error +" - Password wajib diisi !\n";
                }
                if(!et3.getText().toString().equals(et4.getText().toString())){
                    error = error +" - Password dan Confirm Password tidak sama !\n";
                }
                if(error.equals("")){
                    ProgressDialog l = Loading.get(context);
                    l.show();
                    RequestAPIServices x = APIUtilities.getAPIServices();
                    x.update_user(et1.getText().toString(),
                            et3.getText().toString(),
                            et2.getText().toString(),
                            String.valueOf(role.getSelectedItemPosition())
                        ).enqueue(new Callback<GlobalResponse>() {
                        @Override
                        public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                            l.dismiss();
                            if(response.code()==200){
                                if(response.body().getStatus().equals(Constanta.SUCCESS)){
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    ChangeActivity.To(context,Level1.class,pointer);
                                }else{
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<GlobalResponse> call, Throwable t) {
                            l.dismiss();
                            Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    Toast.makeText(context, error, Toast.LENGTH_LONG).show();
                }
            }
        });
        /////////////////////////////////////////////
    }

    @Override
    public void onBackPressed() {
        ChangeActivity.To(context,Level1.class,pointer);
    }
}
package com.ichsan.eappointment.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.ichsan.eappointment.R;
import com.ichsan.eappointment.constant.Constanta;
import com.ichsan.eappointment.retrofit.APIUtilities;
import com.ichsan.eappointment.retrofit.RequestAPIServices;
import com.ichsan.eappointment.retrofit.model.LoginBody;
import com.ichsan.eappointment.utility.ChangeActivity;
import com.ichsan.eappointment.utility.Confirm;
import com.ichsan.eappointment.utility.Loading;
import com.ichsan.eappointment.utility.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private Context context;
    private EditText uid,pass;
    private Button login;
    private CheckBox remember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context= this;
        uid = findViewById(R.id.uid);
        pass = findViewById(R.id.pass);
        login = findViewById(R.id.button);
        remember = findViewById(R.id.remember);

        autoLogin();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uid.getText().toString().length() > 0 && pass.getText().toString().length() > 0){
                    //////// Action to login/////////////
                    RequestAPIServices x = APIUtilities.getAPIServices();
                    ProgressDialog l = Loading.get(context);
                    l.show();
                    x.login(uid.getText().toString(),pass.getText().toString()).enqueue(new Callback<LoginBody>() {
                        @Override
                        public void onResponse(Call<LoginBody> call, Response<LoginBody> response) {
                            l.dismiss();
                            if(response.code()==200){
                                if(response.body().getStatus().equals(Constanta.SUCCESS)) {
                                    response.body().getMessage();
                                    SessionManager.setDataLogin(context,
                                            uid.getText().toString(),
                                            remember.isChecked(),
                                            response.body().getPosition(),
                                            pass.getText().toString());
                                    if (!response.body().getPosition().equals("0")) {
                                        ChangeActivity.To(context, home.class);
                                    } else {
                                        Toast.makeText(context, "Maaf, Akun anda sedang tidak aktif. Mohon menghubungi Administrator.", Toast.LENGTH_SHORT).show();
                                    }
                                }else{
                                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginBody> call, Throwable t) {
                            l.dismiss();
                            Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                        }
                    });

                }else{
                    Toast.makeText(context, "Mohon inputkan User dan Password Anda !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void autoLogin() {
        if( SessionManager.cekLoginFlag(context).equals("true") && SessionManager.cekRemember(context).equals("true") && !SessionManager.getRole(context).equals("0")){
            ChangeActivity.To(context,home.class);
        }else{
            uid.setText(SessionManager.getUname(context));
            pass.setText(SessionManager.getPass(context));
            remember.setChecked(Boolean.parseBoolean(SessionManager.cekRemember(context)));
        }

    }

    @Override
    public void onBackPressed() {
        Confirm.Show("Keluar dari Apliaksi ?",context);
    }
}
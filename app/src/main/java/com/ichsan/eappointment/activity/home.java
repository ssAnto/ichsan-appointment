package com.ichsan.eappointment.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.ichsan.eappointment.R;
import com.ichsan.eappointment.constant.Constanta;
import com.ichsan.eappointment.retrofit.APIUtilities;
import com.ichsan.eappointment.retrofit.RequestAPIServices;
import com.ichsan.eappointment.retrofit.model.GlobalResponse;
import com.ichsan.eappointment.retrofit.model.KetersediaanDsn;
import com.ichsan.eappointment.utility.ChangeActivity;
import com.ichsan.eappointment.utility.Confirm;
import com.ichsan.eappointment.utility.Loading;
import com.ichsan.eappointment.utility.QRScanner;
import com.ichsan.eappointment.utility.SessionManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class home extends AppCompatActivity {

    private TextView title,wlcome;
    private Button button1,button2,button3,button4;
    private ImageView logout;
    private Context context;
    private  Integer stat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = this;
        String role = SessionManager.getRole(context);
        if(role.equals("0")){
            ChangeActivity.To(context,MainActivity.class);
        }

        logout = findViewById(R.id.logout);

        title = findViewById(R.id.title);
        wlcome = findViewById(R.id.welcome);
        title.setText(getString(Constanta.Title[Integer.parseInt(role)]));

        button1 = findViewById(R.id.button1);
        button1.setVisibility(Constanta.VisibleBTN1[Integer.parseInt(role)]);
        button1.setText(getString(Constanta.TxtBTN1[Integer.parseInt(role)]));
        button1.setCompoundDrawablesWithIntrinsicBounds(Constanta.GamabrBTN1[Integer.parseInt(role)],0,0,0);

        button2 = findViewById(R.id.button2);
        button2.setVisibility(Constanta.VisibleBTN2[Integer.parseInt(role)]);
        button2.setText(getString(Constanta.TxtBTN2[Integer.parseInt(role)]));
        button2.setCompoundDrawablesWithIntrinsicBounds(Constanta.GambrBTN2[Integer.parseInt(role)],0,0,0);

        button3 = findViewById(R.id.button3);
        button3.setVisibility(Constanta.VisibleBTN3[Integer.parseInt(role)]);
        button3.setText(getString(Constanta.TxtBTN3[Integer.parseInt(role)]));
        button3.setCompoundDrawablesWithIntrinsicBounds(Constanta.GamabrBTN3[Integer.parseInt(role)],0,0,0);

        button4Setting();

        wlcome.setText(SessionManager.getUname(context));
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!role.equals("2")){
                    ChangeActivity.To(context,Level1.class,"1");
                }else{
                    QRScanner.Scan(context);
                }
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeActivity.To(context,Level1.class,"2");
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeActivity.To(context,Level1.class,"3");
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Confirm.Show("Apakah Anda ingin Logout ? ",context);
            }
        });

    }

    private void button4Setting() {
        if(SessionManager.getRole(context).equals("2")){
            LinearLayout ll = findViewById(R.id.ketersediaan);
            ll.setVisibility(View.VISIBLE);
            button4 = findViewById(R.id.button4);
            button4.setEnabled(false);
            RequestAPIServices x = APIUtilities.getAPIServices();
            ProgressDialog l = Loading.get(context);
            l.show();
            x.status_ketersediaan_dosen(SessionManager.getUname(context)).enqueue(new Callback<KetersediaanDsn>() {
                @Override
                public void onResponse(Call<KetersediaanDsn> call, Response<KetersediaanDsn> response) {
                    l.dismiss();
                    if(response.code() == 200){
                        if(response.body().getStatus().equals(Constanta.SUCCESS)){
                            stat = Integer.parseInt(response.body().getAvailability());
                            button4.setText(Constanta.BTN4TEXT[stat]);
                            button4.setBackground(getDrawable(Constanta.BTN4BG[stat]));
                            button4.setCompoundDrawablesWithIntrinsicBounds(Constanta.BTN4ICON[stat],0,0,0);
                            button4.setEnabled(true);
                        }else{
                            Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        Toast.makeText(context,"Error : "+response.code(), Toast.LENGTH_SHORT).show();
                    }
                }


                @Override
                public void onFailure(Call<KetersediaanDsn> call, Throwable t) {
                    l.dismiss();
                    Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                }
            });
            button4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    l.show();
                    stat = (stat+1)%2;
                    button4.setEnabled(false);
                    x.update_ketersediaan_dosen(SessionManager.getUname(context),
                            stat.toString()).enqueue(new Callback<GlobalResponse>() {
                        @Override
                        public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                            l.dismiss();
                            button4.setEnabled(true);
                            if(response.code() == 200){
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                ChangeActivity.To(context,home.class);
                            }else{
                                Toast.makeText(context,"Error : "+response.code(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<GlobalResponse> call, Throwable t){
                            l.dismiss();
                            button4.setEnabled(true);
                            Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        //LogOut
        Confirm.Show("Apakah Anda ingin Logout ? ",context);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constanta.REQUESTCODE){
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if(result != null){
                sendAbsensi(result.getContents());
            }
        }
    }

    private void sendAbsensi(String qrResult) {
        /// Call API Absen di sini ...
        RequestAPIServices x =  APIUtilities.getAPIServices();
        ProgressDialog l = Loading.get(context);
        l.show();
        x.absen(qrResult,SessionManager.getUname(context)).enqueue(new Callback<GlobalResponse>() {
            @Override
            public void onResponse(Call<GlobalResponse> call, Response<GlobalResponse> response) {
                l.dismiss();
                if(response.code() == 200){
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(context, "Error : "+response.code(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<GlobalResponse> call, Throwable t) {
                l.dismiss();
                Toast.makeText(context, Constanta.ERROR, Toast.LENGTH_SHORT).show();
            }
        });
    }


}
package com.ichsan.eappointment.constant;

import android.view.View;

import com.ichsan.eappointment.R;
import com.ichsan.eappointment.retrofit.model.DatumDosen;

import java.util.ArrayList;
import java.util.HashMap;

public class Constanta {

    public static final String SHARED_PREFERENCES_NAME = "APPOINTMENT";
    public static final String UNAME = "109asskm20";
    public static final String REMEMBER = "123lknklks";
    public static final String ROLE = "50039AKLks";
    public static final String PASS = "099012LKMALxkd";
    public static final String LOGIN_FLAG = "0981mlsdm02";
    public static final String CODE = "VAlUeqwesd";

    public static ArrayList<DatumDosen> tmp = null;

    public static final Integer[] Title = {
            R.string.hello_world,
            R.string.title_admin,
            R.string.title_dosen,
            R.string.title_mhs
    };


    public static final Integer[] VisibleBTN1 = {
            View.GONE,
            View.VISIBLE,
            View.VISIBLE,
            View.VISIBLE
    };
    public static final Integer[] VisibleBTN2 = {
            View.GONE,
            View.VISIBLE,
            View.VISIBLE,
            View.VISIBLE
    };
    public static final Integer[] VisibleBTN3 = {
            View.GONE,
            View.GONE,
            View.VISIBLE,
            View.VISIBLE
    };

    public static final Integer[] TxtBTN1 = {
            R.string.hello_world,
            R.string.adm_button1,
            R.string.dosen_button1,
            R.string.mhs_button1
    };
    public static final Integer[] TxtBTN2 = {
            R.string.hello_world,
            R.string.adm_button2,
            R.string.dosen_button2,
            R.string.mhs_button2
    };
    public static final Integer[] TxtBTN3 = {
            R.string.hello_world,
            R.string.hello_world,
            R.string.dosen_button3,
            R.string.mhs_button3
    };

    public static final Integer[] GAMBARSTATUS = {
            R.drawable.ic_baseline_pending_24,
            R.drawable.ic_baseline_check_circle_24,
            R.drawable.ic_baseline_cancel_24

    };
    public static final Integer[] GamabrBTN1 = {
            R.drawable.ic_baseline_warning_24,
            R.drawable.ic_baseline_person_add_24,
            R.drawable.ic_baseline_qr_code_scanner_24,
            R.drawable.ic_baseline_note_add_24
    };
    public static final Integer[] GambrBTN2 = {
            R.drawable.ic_baseline_warning_24,
            R.drawable.ic_baseline_settings_24,
            R.drawable.ic_baseline_domain_verification_24,
            R.drawable.ic_baseline_history_24
    };
    public static final Integer[] GamabrBTN3 = {
            R.drawable.ic_baseline_warning_24,
            R.drawable.ic_baseline_warning_24,
            R.drawable.ic_baseline_history_24,
            R.drawable.ic_baseline_person_search_24
    };

    public static final  int REQUESTCODE = 0x0000c0de;
    public static final  String SUCCESS = "success";
    public static final  String ERROR = "Mohon Periksa Koneksi Internet";

    public static final String[] OPSI_REGISTER_USER = {
            "-- Pilih Role --",
            "Administrator",
            "Dosen",
            "Mahasiswa"
    };

    public static final String[] OPSI_SETTING_USER = {
            "Not Active",
            "Administrator",
            "Dosen",
            "Mahasiswa"
    };

    public static final String[] STATUS_APPROVAL = {
            "Waiting for Approval",
            "Approved",
            "Rejected"
    };

    public static HashMap<String,Integer> MAPALL;
    static {
        MAPALL = new HashMap<>();
        MAPALL.put("Not Active",0);
        MAPALL.put("Administrator",1);
        MAPALL.put("Dosen",2);
        MAPALL.put("Mahasiswa",3);

        MAPALL.put("Rejected",2);
        MAPALL.put("Approved",1);
        MAPALL.put("Waiting for Approval",0);
    }

    public static final String[] SHIFT = {
            "-- Pilih Shift --",
            "1",
            "2",
            "3",
            "4"
    };

    public static final Integer[] BTN4BG = {
        R.drawable.btn_not_available,
        R.drawable.btn_available,
        R.drawable.btn_not_set_yet
    };

    public static final Integer[] BTN4ICON = {
            R.drawable.ic_not_available,
            R.drawable.ic_available,
            R.drawable.ic_no_data
    };

    public static final String[] BTN4TEXT = {
            "Not Availabel",
            "Availabe",
            "Not Set Yet"
    };

    public static final Integer[] CARDBACKGROUND = {
            R.color.not_available,
            R.color.available,
            R.color.nodata
    };

}



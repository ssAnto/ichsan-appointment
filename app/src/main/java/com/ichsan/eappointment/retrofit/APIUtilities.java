package com.ichsan.eappointment.retrofit;

public class APIUtilities {
    private static String BASE_URL_API = Server.URL;

    public static RequestAPIServices getAPIServices(){
        return RetrofitClient.getClient(BASE_URL_API).create(RequestAPIServices.class);
    }
}

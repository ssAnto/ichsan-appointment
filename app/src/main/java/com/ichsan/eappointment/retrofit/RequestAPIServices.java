package com.ichsan.eappointment.retrofit;


import com.ichsan.eappointment.retrofit.model.DetailAppointment;
import com.ichsan.eappointment.retrofit.model.DetailHistory;
import com.ichsan.eappointment.retrofit.model.GlobalResponse;
import com.ichsan.eappointment.retrofit.model.HistoryAppointment;
import com.ichsan.eappointment.retrofit.model.KetersediaanDsn;
import com.ichsan.eappointment.retrofit.model.ListAppointment;
import com.ichsan.eappointment.retrofit.model.ListDosen;
import com.ichsan.eappointment.retrofit.model.ListKetersediaan;
import com.ichsan.eappointment.retrofit.model.ListUser;
import com.ichsan.eappointment.retrofit.model.LoginBody;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RequestAPIServices {

    ///////////////////// GENERAL ////////////////////////////////
    @FormUrlEncoded
    @POST("/fileta2021/login.php")
    Call<LoginBody> login(@Field("user") String user, @Field("password") String password);

    @FormUrlEncoded
    @POST("/fileta2021/history_appointment.php")
    Call<HistoryAppointment> history_appointment(@Field("userid") String userid,
                                                 @Field("position") String position);

    @FormUrlEncoded
    @POST("/fileta2021/detail_history_appointment.php")
    Call<DetailHistory> detail_history_appointment(@Field("id_appointment") String id_appointment,
                                                   @Field("position") String position);
    //////////////////////////////////////////////////////////////

    /////////////////// DOSEN //////////////////////////////////////
    @FormUrlEncoded
    @POST("/fileta2021/absen.php")
    Call<GlobalResponse> absen(@Field("code_qr") String code_qr,
                               @Field("user") String user);

    @FormUrlEncoded
    @POST("/fileta2021/list_appointment.php")
    Call<ListAppointment> list_appointment(@Field("userid") String userid);

    @FormUrlEncoded
    @POST("/fileta2021/detail_appointment.php")
    Call<DetailAppointment> detail_appointment(@Field("id_appointment") String id_appointment);


    @FormUrlEncoded
    @POST("/fileta2021/approval_appointment.php")
    Call<GlobalResponse> approval_appointment(@Field("id_appointment") String id_appointment,
                                              @Field("approval") String approval,
                                              @Field("tgl_appointment") String tgl_appointment,
                                              @Field("shift") String shift);

    @FormUrlEncoded
    @POST("/fileta2021/status_ketersediaan_dosen.php")
    Call<KetersediaanDsn> status_ketersediaan_dosen(@Field("nip_dsn") String nip_dsn);

    @FormUrlEncoded
    @POST("/fileta2021/update_ketersediaan_dosen.php")
    Call<GlobalResponse> update_ketersediaan_dosen(@Field("nip_dsn") String nip_dsn,
                                                   @Field("availability") String availability);
    /////////////////////////////////////////////////////////////////

    ////////////// MAHASISWA //////////////////////////////////////
    @POST("/fileta2021/list_dosen.php")
    Call<ListDosen> list_dosen();

    @FormUrlEncoded
    @POST("/fileta2021/request_appointment.php")
    Call<GlobalResponse> request_appointment(@Field("userid") String userid,
                                             @Field("nip_dosen") String nip_dosen,
                                             @Field("keterangan") String keterangan);

    @POST("/fileta2021/list_availability_dosen.php")
    Call<ListKetersediaan> list_availability_dosen();
    //////////////////////////////////////////////////////////////

    //////////////// ADMIN //////////////////////////////////////
    @FormUrlEncoded
    @POST("/fileta2021/regis_user.php")
    Call<GlobalResponse> regis_user(@Field("userid") String userid,
                                    @Field("password") String password,
                                    @Field("nama") String nama,
                                    @Field("position") String position);

    @FormUrlEncoded
    @POST("/fileta2021/list_user.php")
    Call<ListUser> list_user(@Field("userid") String userid,@Field("position") String position);

    @FormUrlEncoded
    @POST("/fileta2021/update_user.php")
    Call<GlobalResponse> update_user(@Field("userid") String userid,
                                     @Field("password") String password,
                                     @Field("nama") String nama,
                                     @Field("position") String position);

    ////////////////////////////////////////////////////////////

}

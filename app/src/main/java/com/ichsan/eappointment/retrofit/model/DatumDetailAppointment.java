package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumDetailAppointment {
    @SerializedName("id_appointment")
    @Expose
    private String id_appointment;
    @SerializedName("nama_mhs")
    @Expose
    private String nama_mhs;
    @SerializedName("nip")
    @Expose
    private String nip;
    @SerializedName("keterangan")
    @Expose
    private String keterangan;

    public String getId_appointment() {
        return id_appointment;
    }

    public void setId_appointment(String id_appointment) {
        this.id_appointment = id_appointment;
    }

    public String getNama_mhs() {
        return nama_mhs;
    }

    public void setNama_mhs(String nama_mhs) {
        this.nama_mhs = nama_mhs;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}

package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumDetailHistory {
    @SerializedName("id_appointment")
    @Expose
    private String id_appointment;

    @SerializedName("user_id")
    @Expose
    private String user_id_mhs;

    @SerializedName("nama")
    @Expose
    private String nama_mhs;

    @SerializedName("tgl_appointment")
    @Expose
    private String tgl_appointment;

    @SerializedName("shift")
    @Expose
    private String shift;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("Keterangan")
    @Expose
    private String keterangan;

    public String getUser_id_mhs() {
        return user_id_mhs;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public void setUser_id_mhs(String user_id_mhs) {
        this.user_id_mhs = user_id_mhs;
    }

    public String getId_appointment() {
        return id_appointment;
    }

    public void setId_appointment(String id_appointment) {
        this.id_appointment = id_appointment;
    }

    public String getNama_mhs() {
        return nama_mhs;
    }

    public void setNama_mhs(String nama_mhs) {
        this.nama_mhs = nama_mhs;
    }

    public String getTgl_appointment() {
        return tgl_appointment;
    }

    public void setTgl_appointment(String tgl_appointment) {
        this.tgl_appointment = tgl_appointment;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumDosen {
    @SerializedName("nip")
    @Expose
    private String nip;
    @SerializedName("nama_dsn")
    @Expose
    private String nama_dsn;

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama_dsn() {
        return nama_dsn;
    }

    public void setNama_dsn(String nama_dsn) {
        this.nama_dsn = nama_dsn;
    }
}

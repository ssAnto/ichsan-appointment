package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumListAppointment {

    @SerializedName("id_appointment")
    @Expose
    private String id_appointment;
    @SerializedName("nama_mhs")
    @Expose
    private String nama_mhs;
    @SerializedName("tgl_request")
    @Expose
    private String tgl_request;

    public String getId_appointment() {
        return id_appointment;
    }

    public void setId_appointment(String id_appointment) {
        this.id_appointment = id_appointment;
    }

    public String getNama_mhs() {
        return nama_mhs;
    }

    public void setNama_mhs(String nama_mhs) {
        this.nama_mhs = nama_mhs;
    }

    public String getTgl_request() {
        return tgl_request;
    }

    public void setTgl_request(String tgl_request) {
        this.tgl_request = tgl_request;
    }
}

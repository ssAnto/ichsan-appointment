package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailAppointment extends GlobalResponse {
    @SerializedName("data")
    @Expose
    private DatumDetailAppointment data;

    public DatumDetailAppointment getData() {
        return data;
    }

    public void setData(DatumDetailAppointment data) {
        this.data = data;
    }
}

package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DetailHistory extends GlobalResponse {
    @SerializedName("data")
    @Expose
    private DatumDetailHistory data;

    public DatumDetailHistory getData() {
        return data;
    }

    public void setData(DatumDetailHistory data) {
        this.data = data;
    }
}

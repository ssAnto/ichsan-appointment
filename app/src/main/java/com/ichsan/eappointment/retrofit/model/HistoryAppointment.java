package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HistoryAppointment extends GlobalResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<DatumHistory> data  = new ArrayList<>();

    public ArrayList<DatumHistory> getData() {
        return data;
    }

    public void setData(ArrayList<DatumHistory> data) {
        this.data = data;
    }
}

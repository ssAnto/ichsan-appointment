package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ichsan.eappointment.retrofit.model.DatumListAppointment;
import com.ichsan.eappointment.retrofit.model.GlobalResponse;

import java.util.ArrayList;

public class ListAppointment extends GlobalResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<DatumListAppointment> data  = new ArrayList<>();

    public ArrayList<DatumListAppointment> getData() {
        return data;
    }

    public void setData(ArrayList<DatumListAppointment> data) {
        this.data = data;
    }
}

package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListDosen extends GlobalResponse{
    @SerializedName("data")
    @Expose
    private ArrayList<DatumDosen> data = new ArrayList<>();

    public ArrayList<DatumDosen> getData() {
        return data;
    }

    public void setData(ArrayList<DatumDosen> data) {
        this.data = data;
    }
}

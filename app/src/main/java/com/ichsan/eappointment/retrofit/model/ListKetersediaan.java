package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListKetersediaan extends GlobalResponse{
    @SerializedName("data")
    @Expose
    private ArrayList<DatumKetersediaan> data = new ArrayList<>();

    public ArrayList<DatumKetersediaan> getData() {
        return data;
    }

    public void setData(ArrayList<DatumKetersediaan> data) {
        this.data = data;
    }
}

package com.ichsan.eappointment.retrofit.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListUser extends GlobalResponse{
    @SerializedName("data")
    @Expose
    private ArrayList<DatumUser> data = new ArrayList<>();

    public ArrayList<DatumUser> getData() {
        return data;
    }

    public void setData(ArrayList<DatumUser> data) {
        this.data = data;
    }
}

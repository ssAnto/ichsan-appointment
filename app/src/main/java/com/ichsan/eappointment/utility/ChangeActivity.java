package com.ichsan.eappointment.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.ichsan.eappointment.constant.Constanta;

public class ChangeActivity {
    public static void To(Context context,Class<?>tujuan){
        Intent intent = new Intent(context,tujuan);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        Activity a = (Activity) context;
        a.finish();
    }
    public static void To(Context context,Class<?>tujuan,String button){
        Intent intent = new Intent(context,tujuan);
        intent.putExtra(Constanta.CODE,button);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        Activity a = (Activity) context;
        a.finish();
    }
}

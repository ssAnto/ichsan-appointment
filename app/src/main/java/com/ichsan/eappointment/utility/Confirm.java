package com.ichsan.eappointment.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.ichsan.eappointment.R;
import com.ichsan.eappointment.activity.MainActivity;

public class Confirm {
    public static void  Show(String message, Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("[INFO]")
                .setIcon(R.drawable.ic_baseline_info_24)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(message.contains("Logout")){
                            SessionManager.clearData(context);
                            ChangeActivity.To(context,MainActivity.class);
                        }else{
                            Activity a = (Activity) context;
                            a.finish();
                        }

                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(true)
                .show();
    }
}

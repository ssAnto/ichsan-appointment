package com.ichsan.eappointment.utility;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeConverter {
    public static String forSave(String x){
        SimpleDateFormat norm = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat def = new SimpleDateFormat("yyyy-MM-dd");
        String tmp = "";
        try{
            Date tgl = norm.parse(x);
            if (x.equals("DD-MM-YYYY") ){
                tmp = def.format(new Date().getTime());
            }else{
                tmp = def.format(tgl);
            }
        } catch (Exception e) {
            e.printStackTrace();
            tmp = def.format(new Date().getTime());
        }
        return tmp+" 00:00:00";
    }
}

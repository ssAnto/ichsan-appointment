package com.ichsan.eappointment.utility;

import android.app.ProgressDialog;
import android.content.Context;

import com.ichsan.eappointment.R;

public class Loading {

    public static ProgressDialog get (Context context){
        ProgressDialog loading = new ProgressDialog(context);
        loading.setMessage(context.getResources().getString(R.string.loading));
        loading.setCancelable(false);
        return loading;
    }
}

package com.ichsan.eappointment.utility;

import android.app.Activity;
import android.content.Context;
import com.google.zxing.integration.android.IntentIntegrator;

public class QRScanner {
    public static void Scan(Context context){
        /////// Start Scan ////////////////////////
        Activity activity = (Activity) context;
        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setOrientationLocked(false);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
        //////////////////////////////////////////
    }
}

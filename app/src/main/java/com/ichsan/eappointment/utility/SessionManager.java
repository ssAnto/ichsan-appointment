package com.ichsan.eappointment.utility;


import android.content.Context;
import android.content.SharedPreferences;

import com.ichsan.eappointment.constant.Constanta;


public class SessionManager  {

    //konstruktor untuk menggunakan shared preferences
    protected static SharedPreferences retrieveSharedPreferences(Context context) {
        return context.getSharedPreferences(Constanta.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    //untuk CRUD
    protected static SharedPreferences.Editor retrieveSharedPreferencesEditor(Context context)
    {
        return retrieveSharedPreferences(context).edit();
    }

    //procedure sesuai kebutuhan
    public static void setDataLogin(Context context,
                                    String uname,
                                    Boolean remember,
                                    String role,
                                    String Password){

        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);

        editor.putString(Constanta.UNAME,uname);
        editor.putString(Constanta.REMEMBER,String.valueOf(remember));
        editor.putString(Constanta.ROLE,role);
        editor.putString(Constanta.PASS,Password);
        editor.putString(Constanta.LOGIN_FLAG,"true");
        editor.commit();
    }

    //cek flag login
    public static String cekLoginFlag(Context context)
    {
        return retrieveSharedPreferences(context).getString(Constanta.LOGIN_FLAG,"false");
    }

    //ambil uname
    public static String getUname(Context context)
    {
        return retrieveSharedPreferences(context).getString(Constanta.UNAME,"");
    }
    //ambil password
    public static String getPass(Context context)
    {
        return retrieveSharedPreferences(context).getString(Constanta.PASS,"");
    }
    //cek remember
    public static String cekRemember(Context context)
    {
        return retrieveSharedPreferences(context).getString(Constanta.REMEMBER,"false");
    }
    //logout
    public static void clearData(Context context)
    {
        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);

        if(Boolean.parseBoolean(cekRemember(context))){
            editor.putString(Constanta.LOGIN_FLAG, "false");
        }
        else {
            editor.clear();
        }
        editor.commit();
    }

    public static void setRole(Context context, String role) {
        SharedPreferences.Editor editor = retrieveSharedPreferencesEditor(context);
        editor.putString(Constanta.ROLE,role);
        editor.commit();
    }

    public static String getRole(Context context){
        return retrieveSharedPreferences(context).getString(Constanta.ROLE,"0");
    }



}

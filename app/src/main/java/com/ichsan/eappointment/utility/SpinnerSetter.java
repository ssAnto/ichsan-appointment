package com.ichsan.eappointment.utility;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class SpinnerSetter {
    public static void Set(Context context, String[] pertanyaan, Spinner spinner){
        ArrayAdapter<String> adapterPertanyaanDuaDelapan = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, pertanyaan);
        adapterPertanyaanDuaDelapan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterPertanyaanDuaDelapan);
    }
}

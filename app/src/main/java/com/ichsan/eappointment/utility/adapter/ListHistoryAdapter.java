package com.ichsan.eappointment.utility.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.ichsan.eappointment.R;
import com.ichsan.eappointment.activity.Level2;
import com.ichsan.eappointment.constant.Constanta;
import com.ichsan.eappointment.retrofit.model.DatumHistory;
import com.ichsan.eappointment.utility.ChangeActivity;
import com.ichsan.eappointment.utility.SessionManager;

import java.util.ArrayList;

public class ListHistoryAdapter extends RecyclerView.Adapter<ListHistoryAdapter.ViewHolder>{
    private ArrayList<DatumHistory> datumArrayList;
    private LinearLayout listCard;
    private LinearLayout listSurvey;
    private MaterialRippleLayout rippleLayout;

    public ListHistoryAdapter(ArrayList<DatumHistory> datumArrayList ) {
        this.datumArrayList = datumArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_layout, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.text1.setText(datumArrayList.get(position).getId_appointment());
        holder.text2.setText(datumArrayList.get(position).getNama_mhs());
        try {
            holder.text3.setText(Constanta.STATUS_APPROVAL[Integer.parseInt(datumArrayList.get(position).getStatus())]);
        }catch (Exception x){
            holder.text3.setText("Pending Approval");
        }
        holder.text4.setText("Tgl : "+datumArrayList.get(position).getTgl_appointment()+", Shift : "+datumArrayList.get(position).getShift());
    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView text1, text2, text3,text4;

        public ViewHolder(View itemView) {
            super(itemView);

            text1 =  itemView.findViewById(R.id.text1);
            text2 =  itemView.findViewById(R.id.text2);
            text3 =  itemView.findViewById(R.id.text3);
            text4 = itemView.findViewById(R.id.text4);
            listSurvey = (LinearLayout) itemView.findViewById(R.id.historySurvey);
            listCard =  (LinearLayout) itemView.findViewById(R.id.list_item);
            rippleLayout = (MaterialRippleLayout) itemView.findViewById(R.id.ripple_historysurvey_cmo);

            rippleLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            listSurvey.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Context context = v.getContext();
                    String button = "2";
                    if(SessionManager.getRole(context).equals("2")){
                        button="3";
                    }
                    String tmp = button+" "+text1.getText().toString();
                    if(position != RecyclerView.NO_POSITION){
                        ChangeActivity.To(context, Level2.class,tmp);
                    }
                }
            });

        }
    }
}

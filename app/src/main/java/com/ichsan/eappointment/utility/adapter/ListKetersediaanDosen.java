package com.ichsan.eappointment.utility.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.ichsan.eappointment.R;
import com.ichsan.eappointment.activity.Level2;
import com.ichsan.eappointment.constant.Constanta;
import com.ichsan.eappointment.retrofit.model.DatumKetersediaan;
import com.ichsan.eappointment.retrofit.model.DatumListAppointment;
import com.ichsan.eappointment.utility.ChangeActivity;

import java.util.ArrayList;

public class ListKetersediaanDosen extends RecyclerView.Adapter<ListKetersediaanDosen.ViewHolder>{
    private ArrayList<DatumKetersediaan> datumArrayList;
    private Context context;


    public ListKetersediaanDosen(ArrayList<DatumKetersediaan> datumArrayList, Context context ) {
        this.datumArrayList = datumArrayList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_available_dsn, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.text1.setText(datumArrayList.get(position).getNip());
        holder.text2.setText(datumArrayList.get(position).getNama());
        holder.text3.setText(Constanta.BTN4TEXT[Integer.parseInt(datumArrayList.get(position).getAvailability())]);
        holder.listCard.setBackground(context.getDrawable(Constanta.BTN4BG[Integer.parseInt(datumArrayList.get(position).getAvailability())]));
        holder.x.setImageDrawable(context.getDrawable(Constanta.BTN4ICON[Integer.parseInt(datumArrayList.get(position).getAvailability())]));
    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView text1, text2,text3;
        private ImageView x ;
        private LinearLayout listSurvey;
        private MaterialRippleLayout rippleLayout;
        private LinearLayout listCard;

        public ViewHolder(View itemView) {
            super(itemView);
            text1 =  itemView.findViewById(R.id.text1);
            text2 =  itemView.findViewById(R.id.text2);
            text3 = itemView.findViewById(R.id.text3);
            x = itemView.findViewById(R.id.imageView3);
            listSurvey = (LinearLayout) itemView.findViewById(R.id.historySurvey);
            listCard = (LinearLayout) itemView.findViewById(R.id.list_item);
            rippleLayout = (MaterialRippleLayout) itemView.findViewById(R.id.ripple_historysurvey_cmo);



        }
    }
}

package com.ichsan.eappointment.utility.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.ichsan.eappointment.R;
import com.ichsan.eappointment.activity.Level2;
import com.ichsan.eappointment.constant.Constanta;
import com.ichsan.eappointment.retrofit.model.DatumUser;
import com.ichsan.eappointment.utility.ChangeActivity;

import java.util.ArrayList;

public class ListUserAdapter extends RecyclerView.Adapter<ListUserAdapter.ViewHolder>{
    private ArrayList<DatumUser> datumArrayList;
    private LinearLayout listCard;
    private LinearLayout listSurvey;
    private MaterialRippleLayout rippleLayout;

    public ListUserAdapter(ArrayList<DatumUser> datumArrayList ) {
        this.datumArrayList = datumArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_layout, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.text1.setText(datumArrayList.get(position).getNip());
        holder.text2.setText(datumArrayList.get(position).getNama());
        holder.text3.setText(Constanta.OPSI_SETTING_USER[Integer.parseInt(datumArrayList.get(position).getPosition())]);
    }

    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView text1, text2, text3,text4;

        public ViewHolder(View itemView) {
            super(itemView);

            text1 =  itemView.findViewById(R.id.text1);
            text2 =  itemView.findViewById(R.id.text2);
            text3 =  itemView.findViewById(R.id.text3);
            listSurvey = (LinearLayout) itemView.findViewById(R.id.historySurvey);
            listCard = (LinearLayout) itemView.findViewById(R.id.list_item);
            rippleLayout = (MaterialRippleLayout) itemView.findViewById(R.id.ripple_historysurvey_cmo);

            rippleLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            listSurvey.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Context context = v.getContext();
                    String tmp = "2 "+text1.getText().toString()+" "+text2.getText().toString()+" "+Constanta.MAPALL.get(text3.getText().toString());
                    if(position != RecyclerView.NO_POSITION){
                        ChangeActivity.To(context, Level2.class,tmp);
                    }
                }
            });

        }
    }
}
